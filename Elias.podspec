Pod::Spec.new do |s|

  s.name         = "Elias"
  s.version      = "1.0.0"
  s.summary      = "A short description of Elias because its imple generator of the view app"

  s.description  = "Simple description string it is"
  s.homepage     = "https://gitlab.com/xcodewarrior/elias"

  s.license      = "MIT"

  s.author             = { "eliakorkmaz" => "emrahkrkmz1@gmail.com" }
  s.platform     = :ios, "12.0"

  s.source       = { :git => "https://gitlab.com/xcodewarrior/elias.git", :tag => "1.0.0" }

  s.source_files  = "Elias/**/*"

end
